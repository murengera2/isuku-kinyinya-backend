from django.urls import path
from users.views import UserList, UserDetail,PositionList,PositionDetail


urlpatterns = [
    path('users/', UserList.as_view(), name='users_list'),
    path('positions/', PositionList.as_view(), name='positions_list'),

    path('users/<slug:pk>', UserDetail.as_view(), name='users_detail'),
    path('positions/<slug:pk>', PositionDetail.as_view(), name='positions_detail')
]