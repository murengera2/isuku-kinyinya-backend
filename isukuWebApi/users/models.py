from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from django.contrib.auth.models import User
from users.manager import UserManager

import uuid
import datetime

class Position(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    title = models.CharField(max_length=200, blank=False, editable=True)
    description = models.CharField(max_length=500, null=True,blank=True)

    def __str__(self):
        return self.title

#custom user/manager registration model
class User(AbstractUser):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    name = models.CharField(max_length=200, editable=True, blank=False, null=False)
    phone_number = models.CharField(max_length=15, unique=True)
    image = models.ImageField(upload_to='images/profile')
    email = models.EmailField(max_length=200, unique=True, null=False, blank=False)
    statues = {
        ('ACTIVE', 'ACTIVE'),
        ('INACTIVE', 'INACTIVE'),
    }
    
    status = models.CharField(max_length=50, choices=statues, default='INACTIVE')
    position = models.ForeignKey(Position, on_delete=models.CASCADE, null=True,blank=True)
    registered_time = models.DateTimeField(auto_now_add=True, editable=False)

    USERNAME_FIELD = 'phone_number'

    objects = UserManager()

    def save(self,*args, **kwargs):
        if self.phone_number:
            self.username = self.phone_number
    
        super(User,self).save(*args, **kwargs)
    
    def __str__(self):
        return self.name
