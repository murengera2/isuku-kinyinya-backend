from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from users.models import User, Position

class PositionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Position
        fields = ('id', 'title', 'description')

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('__all__')
    
    def to_representation(self, instance):
        serialized_data = super(UserSerializer, self).to_representation(instance)
        serialized_data['position'] = PositionSerializer(instance.position).data
        return serialized_data
        