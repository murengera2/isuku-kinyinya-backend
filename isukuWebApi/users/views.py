from django.shortcuts import render
from users.models import Position, User
from users.serializers import PositionSerializer, UserSerializer
from rest_framework import response,request
from rest_framework.response import Response
from rest_framework import generics,mixins
# Create your views here.

class UserList(mixins.ListModelMixin,
                    mixins.CreateModelMixin,
                    generics.GenericAPIView):

    queryset = User.objects.all()
    serializer_class = UserSerializer
    # permission_classes = (IsAuthenticated,)

    def post(self,request,*args, **kwargs):
        return self.create(request, *args, **kwargs)
        
    def get(self,request,*args, **kwargs):
        return self.list(request,*args, **kwargs)

class UserDetail(mixins.
                    RetrieveModelMixin,mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,generics.GenericAPIView):
    
    queryset = User.objects.all()
    serializer_class = UserSerializer
    # permission_classes = (IsAuthenticated,)

    def get(self,request,*args, **kwargs):
        return self.retrieve(request, *args, **kwargs)
        
    def patch(self,request,*args, **kwargs):
        return self.partial_update(request,*args, **kwargs)

    def delete(self,request,*args, **kwargs):
        return self.destroy(request, *args, **kwargs)

class PositionList(mixins.ListModelMixin,
                    mixins.CreateModelMixin,
                    generics.GenericAPIView):

    queryset = Position.objects.all()
    serializer_class = PositionSerializer
    # permission_classes = (IsAuthenticated,)

    def post(self,request,*args, **kwargs):
        return self.create(request, *args, **kwargs)
        
    def get(self,request,*args, **kwargs):
        return self.list(request, *args, **kwargs)
        
class PositionDetail(mixins.
                    RetrieveModelMixin,mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,generics.GenericAPIView):
    
    queryset = Position.objects.all()
    serializer_class = PositionSerializer
    # permission_classes = (IsAuthenticated,)

    def get(self,request,*args, **kwargs):
        return self.retrieve(request, *args, **kwargs)
        
    def patch(self,request,*args, **kwargs):
        return self.partial_update(request,*args, **kwargs)

    def delete(self,request,*args, **kwargs):
        return self.destroy(request, *args, **kwargs)