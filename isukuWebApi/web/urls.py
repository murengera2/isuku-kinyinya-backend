from django.urls import path
from web.views import *


urlpatterns = [
    path('services/', ServiceList.as_view(), name='services_list'),
    path('gallerys/', GalleryList.as_view(), name='Gallerys_list'),
    path('announcements/', AnnouncementList.as_view(), name='announcements_list'),
    path('missions/', MissionList.as_view(), name='missions_list'),
    path('visions/', VisionList.as_view(), name='visions_list'),
    path('values/', ValueList.as_view(), name='values_list'),
    path('companys/', CompanyList.as_view(), name='companys_list'),
    path('services/<slug:pk>', ServiceDetail.as_view(), name='service_detail'),
    path('gallerys/<slug:pk>', GalleryDetail.as_view(), name='gallery_detail'),
    path('announcements/<slug:pk>', AnnoncementDetail.as_view(), name='announcement_detail'),
    path('missions/<slug:pk>', MissionDetail.as_view(), name='missions_detail'),
    path('visions/<slug:pk>', VisionDetail.as_view(), name='visions_detail'),
    path('values/<slug:pk>', ValueDetail.as_view(), name='values_detail'),
    path('companys/<slug:pk>', CompanyDetail.as_view(), name='companys_detail'),
    
]