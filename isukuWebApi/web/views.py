from django.shortcuts import render
from rest_framework.permissions import AllowAny,IsAuthenticated
from web.models import Gallery, Service,Announcement,Mission,Value,Company,Vision
from web.serializers import GallerySerializer, ServiceSerializer,AnnouncementSerializer,ValueSerializer,MissionSerializer,CompanySerializer,VisionSerializer
from rest_framework import response, request
from rest_framework.decorators import api_view,permission_classes
from rest_framework.response import Response
from rest_framework import generics,mixins
# Create your views here.


@api_view(['GET'])
def welcome_to_Isuku_Kinyinya(request):
    return Response("Murakaza neza ku ISUKU KINYINYA COMPANY LTD")
    pass

class ServiceList(mixins.ListModelMixin,
                    mixins.CreateModelMixin,
                    generics.GenericAPIView):

    queryset = Service.objects.all()
    serializer_class = ServiceSerializer
    # permission_classes = (IsAuthenticated,)

    def post(self,request,*args, **kwargs):
        return self.create(request, *args, **kwargs)
        
    def get(self,request,*args, **kwargs):
        return self.list(request,*args, **kwargs)

class ServiceDetail(mixins.
                    RetrieveModelMixin,mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,generics.GenericAPIView):
    
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer
    # permission_classes = (IsAuthenticated,)

    def get(self,request,*args, **kwargs):
        return self.retrieve(request, *args, **kwargs)
        
    def patch(self,request,*args, **kwargs):
        return self.partial_update(request,*args, **kwargs)

    def delete(self,request,*args, **kwargs):
        return self.destroy(request, *args, **kwargs)

class GalleryList(mixins.ListModelMixin,
                    mixins.CreateModelMixin,
                    generics.GenericAPIView):

    queryset = Gallery.objects.all()
    serializer_class = GallerySerializer
    # permission_classes = (IsAuthenticated,)

    def post(self,request,*args, **kwargs):
        return self.create(request, *args, **kwargs)
        
    def get(self,request,*args, **kwargs):
        return self.list(request, *args, **kwargs)
        
class GalleryDetail(mixins.
                    RetrieveModelMixin,mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,generics.GenericAPIView):
    
    queryset = Gallery.objects.all()
    serializer_class = GallerySerializer
    # permission_classes = (IsAuthenticated,)

    def get(self,request,*args, **kwargs):
        return self.retrieve(request, *args, **kwargs)
        
    def patch(self,request,*args, **kwargs):
        return self.partial_update(request,*args, **kwargs)

    def delete(self,request,*args, **kwargs):
        return self.destroy(request, *args, **kwargs)

class AnnouncementList(mixins.ListModelMixin,
                    mixins.CreateModelMixin,
                    generics.GenericAPIView):

    queryset = Announcement.objects.all()
    serializer_class = AnnouncementSerializer
    # permission_classes = (IsAuthenticated,)

    def post(self,request,*args, **kwargs):
        return self.create(request, *args, **kwargs)
        
    def get(self,request,*args, **kwargs):
        return self.list(request, *args, **kwargs)
        
class AnnoncementDetail(mixins.
                    RetrieveModelMixin,mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,generics.GenericAPIView):
    
    queryset = Announcement.objects.all()
    serializer_class = AnnouncementSerializer
    # permission_classes = (IsAuthenticated,)

    def get(self,request,*args, **kwargs):
        return self.retrieve(request, *args, **kwargs)
        
    def patch(self,request,*args, **kwargs):
        return self.partial_update(request,*args, **kwargs)

    def delete(self,request,*args, **kwargs):
        return self.destroy(request, *args, **kwargs)

class ValueList(mixins.ListModelMixin,
                    mixins.CreateModelMixin,
                    generics.GenericAPIView):

    queryset = Value.objects.all()
    serializer_class = ValueSerializer
    # permission_classes = (IsAuthenticated,)

    def post(self,request,*args, **kwargs):
        return self.create(request, *args, **kwargs)
        
    def get(self,request,*args, **kwargs):
        return self.list(request, *args, **kwargs)
        
class ValueDetail(mixins.
                    RetrieveModelMixin,mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,generics.GenericAPIView):
    
    queryset = Value.objects.all()
    serializer_class = ValueSerializer
    # permission_classes = (IsAuthenticated,)

    def get(self,request,*args, **kwargs):
        return self.retrieve(request, *args, **kwargs)
        
    def patch(self,request,*args, **kwargs):
        return self.partial_update(request,*args, **kwargs)

    def delete(self,request,*args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class MissionList(mixins.ListModelMixin,
                    mixins.CreateModelMixin,
                    generics.GenericAPIView):

    queryset = Mission.objects.all()
    serializer_class = MissionSerializer
    # permission_classes = (IsAuthenticated,)

    def post(self,request,*args, **kwargs):
        return self.create(request, *args, **kwargs)
        
    def get(self,request,*args, **kwargs):
        return self.list(request, *args, **kwargs)
        
class MissionDetail(mixins.
                    RetrieveModelMixin,mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,generics.GenericAPIView):
    
    queryset = Mission.objects.all()
    serializer_class = MissionSerializer
    # permission_classes = (IsAuthenticated,)

    def get(self,request,*args, **kwargs):
        return self.retrieve(request, *args, **kwargs)
        
    def patch(self,request,*args, **kwargs):
        return self.partial_update(request,*args, **kwargs)

    def delete(self,request,*args, **kwargs):
        return self.destroy(request, *args, **kwargs)

class VisionList(mixins.ListModelMixin,
                    mixins.CreateModelMixin,
                    generics.GenericAPIView):

    queryset = Vision.objects.all()
    serializer_class = VisionSerializer
    # permission_classes = (IsAuthenticated,)

    def post(self,request,*args, **kwargs):
        return self.create(request, *args, **kwargs)
        
    def get(self,request,*args, **kwargs):
        return self.list(request, *args, **kwargs)
        
class VisionDetail(mixins.
                    RetrieveModelMixin,mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,generics.GenericAPIView):
    
    queryset = Vision.objects.all()
    serializer_class = VisionSerializer
    # permission_classes = (IsAuthenticated,)

    def get(self,request,*args, **kwargs):
        return self.retrieve(request, *args, **kwargs)
        
    def patch(self,request,*args, **kwargs):
        return self.partial_update(request,*args, **kwargs)

    def delete(self,request,*args, **kwargs):
        return self.destroy(request, *args, **kwargs)

class CompanyList(mixins.ListModelMixin,
                    mixins.CreateModelMixin,
                    generics.GenericAPIView):

    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    # permission_classes = (IsAuthenticated,)

    def post(self,request,*args, **kwargs):
        return self.create(request, *args, **kwargs)
        
    def get(self,request,*args, **kwargs):
        return self.list(request, *args, **kwargs)
        
class CompanyDetail(mixins.
                    RetrieveModelMixin,mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,generics.GenericAPIView):
    
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    # permission_classes = (IsAuthenticated,)

    def get(self,request,*args, **kwargs):
        return self.retrieve(request, *args, **kwargs)
        
    def patch(self,request,*args, **kwargs):
        return self.partial_update(request,*args, **kwargs)

    def delete(self,request,*args, **kwargs):
        return self.destroy(request, *args, **kwargs)