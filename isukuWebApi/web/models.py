from django.db import models
from users.models import User

# Create your models here.
class Service(models.Model):
    title = models.CharField(max_length=100, null=False, blank=False)
    service_image = models.ImageField(upload_to='images/service')
    description = models.CharField(max_length=500,blank=True,null=True)
    created_at=models.DateField(auto_now_add=True,null=False,blank=False)
    
    def __str__(self):
        return self.title

class Announcement(models.Model):
    title = models.CharField(max_length=100, null=False, blank=False)
    description = models.CharField(max_length=500, blank=True, null=True)
    link = models.CharField(max_length=200, null=True, blank=True)
    postion = models.CharField(max_length=300, null=True, blank=True)
    requirement = models.CharField(max_length=500, null=True,blank=True)
    created_by = models.ForeignKey(User,on_delete=models.CASCADE,null=True,blank=True)
    categories = {
        ('OLD', 'OLD'),
        ('NEW', 'NEW'),
    }

    category = models.CharField(max_length=50, choices=categories, default='NEW')
    created_at = models.DateField(auto_now_add=True,null=False,blank=False)

    def __str__(self):
        return self.title
    

class Gallery(models.Model):
    title = models.CharField(max_length=100,null=False,blank=False)
    gallery_image = models.ImageField(upload_to='images/gallery')

    def __str__(self):
        return self.title

class Value(models.Model):
    title = models.CharField(max_length=100, null=False, blank=False)
    description = models.CharField(max_length=500,blank=True,null=True)
    created_at = models.DateField(auto_now_add=True,null=False,blank=False)
    
    def __str__(self):
        return self.title

class Mission(models.Model):
    title = models.CharField(max_length=100, null=False, blank=False)
    description = models.CharField(max_length=500,blank=True,null=True)
    created_at = models.DateField(auto_now_add=True,null=False,blank=False)
    
    def __str__(self):
        return self.title

class Vision(models.Model):
    title = models.CharField(max_length=100, null=False, blank=False)
    description = models.CharField(max_length=500,blank=True,null=True)
    created_at = models.DateField(auto_now_add=True,null=False,blank=False)
    
    def __str__(self):
        return self.title

class Company(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    logo = models.ImageField(upload_to='images/company')
    description = models.CharField(max_length=500,blank=True,null=True)
    created_at = models.DateField(auto_now_add=True,null=False,blank=False)
    
    def __str__(self):
        return self.name