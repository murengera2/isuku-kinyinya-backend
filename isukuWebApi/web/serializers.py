from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from users.serializers import UserSerializer
from web.models import Service,Announcement,Gallery,Mission,Vision,Value,Company



class GallerySerializer(serializers.ModelSerializer):
    class Meta:
        model = Gallery
        fields = ('__all__')

class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        fields = ('__all__')

class MissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mission
        fields = ('__all__')

class VisionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vision
        fields = ('__all__')

class ValueSerializer(serializers.ModelSerializer):
    class Meta:
        model = Value
        fields = ('__all__')

class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('__all__')

class AnnouncementSerializer(serializers.ModelSerializer):
    class Meta: 
        model = Announcement
        fields = ('__all__')
    
    def to_representation(self, instance):
        serialized_data = super(AnnouncementSerializer, self).to_representation(instance)
        serialized_data['created_by'] = UserSerializer(instance.created_by).data
        return serialized_data