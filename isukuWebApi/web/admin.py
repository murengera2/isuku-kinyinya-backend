from django.contrib import admin
from web.models import Service,Gallery,Announcement,Mission,Vision,Value,Company

admin.site.register(Service)
admin.site.register(Gallery)
admin.site.register(Announcement)
admin.site.register(Mission)
admin.site.register(Vision)
admin.site.register(Value)
admin.site.register(Company)